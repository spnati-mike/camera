var button = document.getElementById('button');
var flare = document.getElementById('flash-circle');
var warning = document.getElementById('warning');

button.onclick = function() {
  warning.style.backgroundColor = 'red';
  setTimeout(function () {
    flare.style.opacity = '1';
    setTimeout(function () {
      flare.style.opacity = '0';
      warning.style.backgroundColor = 'black';
    }, 100);
  }, 50);

}
